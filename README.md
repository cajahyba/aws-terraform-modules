# aws-terraform-example

## Create a Terraform user

1. In the AWS Console home, look for IAM on the top search box.
2. Select IAM on the list
3. On the left Menu, look for Users and click on it
4. Click on the option to add a new user on the top right
5. Inform "terraform-user" on the "User name" text box
6. Click on next
7. In "Permission options" session, click on "Attach policies" directly
8. A new session "Permissions policies" will be shown
9. Look for "AdministratorAccess" policy name (search for it if necessary) and select it
10. Click in "Next"
11. A "Review and create" page will be shown. You can review the info, and, if everything is OK, then click on "Create user"

## Create an Access Key for the Terraform-User

info here


## Configure aws-cli to use terraform-user credentials

info here 


## Create a bucket to store Terraform State

More Info about [Terraform State](https://developer.hashicorp.com/terraform/language/state)

Info here


## Create a DynamoDB table to lock Terraform State 

This way we can avoid 2 people changing the terraform state at the same time.

