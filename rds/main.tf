# create database subnet group
resource "aws_db_subnet_group" "database_subnet_group" {
  name        = "${var.project_name}-${var.environment}-database-subnets"
  subnet_ids  = [var.private_data_subnet_az1_id, var.private_data_subnet_az2_id]
  description = "subnets for database instance"

  tags = {
    Name = "${var.project_name}-${var.environment}-database-subnets"
  }
}

# # get information about a database snapshot
# # Just use this if you want to create your database and load a snapshot
# data "aws_db_snapshot" "latest_db_snapshot" {
#   db_snapshot_identifier = var.database_snapshot_identifier
#   most_recent            = true
#   snapshot_type          = manual
# }

# launch an rds instance from a database snapshot
resource "aws_db_instance" "database_instance" {
  engine            = "mysql"
  engine_version    = "8.0.31"
  instance_class    = var.database_instance_class
  db_name           = var.db_name
  username          = var.db_username
  password          = var.db_password
  allocated_storage = var.db_allocated_storage
  availability_zone = var.availability_zone_1
  identifier        = var.database_instance_identifier
  multi_az          = var.multi_az_deployment


  db_subnet_group_name   = aws_db_subnet_group.database_subnet_group.name
  vpc_security_group_ids = [var.database_security_group_id]
  skip_final_snapshot    = true
  # snapshot_identifier    = aws_db_subnet_group.database_subnet_group.id # Uncomment here if you are using a database snapshot
}