# RDS variables
variable "project_name" {}
variable "environment" {}

variable "private_data_subnet_az1_id" {}
variable "private_data_subnet_az2_id" {}

variable "database_snapshot_identifier" {}
variable "database_instance_class" {}
variable "database_instance_identifier" {}

variable "availability_zone_1" {}
variable "multi_az_deployment" {}
variable "database_security_group_id" {}

variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "db_allocated_storage" {}





